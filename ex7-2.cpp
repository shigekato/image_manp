#include <stdio.h>
#include "defs.h"
#include "image.h"
#include "bitseq.h"


using namespace Sousei;


// メイン関数： ここから処理を開始
int main(int argc, char** argv)
{
	const int n = 3; // [パラメータ]圧縮画像では解像度を幅・高さともに 1/n 倍する

	if (argc < 3) {
		fprintf(stderr, "実行方法: ./ex7-2 [圧縮ファイル] [復元画像]\n");
		return 1;
	}

	// 入力ファイルからビット列を読み込む
	BitSeq seq;
	seq.Load(argv[1]);

	// ビット列の 0 ビット目から 32 ビット分を抜き出す ⇒ 元画像の幅を表す整数値に変換
	BitSeq Ws = seq.SubSeq(0, 32);
	int W = ToInt32(Ws);

	// ビット列の 32 ビット目から 32 ビット分を抜き出す ⇒ 元画像の高さを表す整数値に変換
	BitSeq Hs = seq.SubSeq(32, 32);
	int H = ToInt32(Hs);

	// 幅 W/n, 高さ H/n の低解像度画像を用意
	int Wo = (W + n - 1) / n; // n-1 を足しているのは小数点以下切り上げ処理のため
	int Ho = (H + n - 1) / n;
	Image S(Wo, Ho);

	// ビット列の 64 ビット目以降から画素値を読み出し，低解像度画像にセット
	int m = 64;
	for (int y = 0; y < Ho; ++y) {
		for (int x = 0; x < Wo; ++x) {
			BitSeq Rs = seq.SubSeq(m, 8); // 8 ビットずつ読み出す
			m += 8;
			BitSeq Gs = seq.SubSeq(m, 8);
			m += 8;
			BitSeq Bs = seq.SubSeq(m, 8);
			m += 8;
			S(x, y, R) = ToInt8(Rs); // 色を表す整数値に変換し，各ピクセルにセット
			S(x, y, G) = ToInt8(Gs);
			S(x, y, B) = ToInt8(Bs);
		}
	}

	// 幅 W, 高さ H の復元画像を用意
	Image I(W, H);

	// 低解像度画像の画素値を復元画像にコピー
	for (int y = 0; y < H; ++y) {
		for (int x = 0; x < W; ++x) {
			I(x, y, R) = S(x / n, y / n, R); // int型同士の割り算では自動的に小数点以下切り捨てとなる
			I(x, y, G) = S(x / n, y / n, G);
			I(x, y, B) = S(x / n, y / n, B);
		}
	}

	// 画像を表示
	I.Show();

	// 画像を保存
	I.Save(argv[2]);

	return 0;
}
