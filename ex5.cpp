#include <stdio.h>
#include "defs.h"
#include "image.h"
#include "bitseq.h"


using namespace Sousei;


// メイン関数： ここから処理を開始
int main(int argc, char** argv)
{
	if (argc < 3) {
		fprintf(stderr, "実行方法: ./ex5 [入力画像] [出力データファイル]\n");
		return 1;
	}

	// 入力画像を読み込む
	Image I;
	if (I.Load(argv[1]) != SOUSEI_OK) {
		fprintf(stderr, "エラー： 入力画像の読み込みに失敗しました．\n");
		return 1;
	}

	// 出力用ビット列を用意（最初は空）
	BitSeq seq;

	// 画像の幅と高さを取得
	int W = I.Width();
	int H = I.Height();

	// 幅と高さの値を32ビットのビット列に変換し，出力用ビット列に連結
	// なお，.Print() は全て確認用の出力なので，消してもOK
	BitSeq Ws = To32BitCode(W);
	BitSeq Hs = To32BitCode(H);
	Ws.Print();
	Hs.Print();
	seq.Print();
	seq += Ws; // 現在の seq に Ws を連結
	seq.Print();
	seq += Hs; // 現在の seq に Hs を連結
	seq.Print();

	// 各ピクセルに対し・・・
	for (int y = 0; y < H; ++y) {
		for (int x = 0; x < W; ++x) {
			// R, G, B の値をそれぞれ8ビットのビット列に変換し，出力用ビット列に連結
			BitSeq Rs = To8BitCode(I(x, y, R));
			BitSeq Gs = To8BitCode(I(x, y, G));
			BitSeq Bs = To8BitCode(I(x, y, B));
			seq += Rs;
			seq += Gs;
			seq += Bs;
		}
	}

	// 出力用ビット列をデータファイルに保存
	seq.Save(argv[2]);

	return 0;
}
