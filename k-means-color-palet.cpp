#include <stdio.h>
#include "defs.h"
#include "image.h"
#include "tools.h"
#include "bitseq.h"


using namespace Sousei;


// メイン関数： ここから処理を開始
int main(int argc, char** argv)
{
    if (argc < 3) {
        fprintf(stderr, "実行方法: ./ex12-1 [入力画像] [圧縮ファイル]\n");
        return 1;
    }

    // 入力画像を読み込む
    Image I;
    if (I.Load(argv[1]) != SOUSEI_OK) {
        fprintf(stderr, "エラー： 入力画像の読み込みに失敗しました．\n");
        return 1;
    }

    // 入力画像の幅と高さを取得
    int W = I.Width();
    int H = I.Height();

    // カラーパレットを用意
    ColorPalette Palette;

        // TODO: k-mens法を用いて代表色を選ぶ I -> [][r, g, b]


    // 出力用ビット列を用意
    BitSeq seq;

    // 画像の幅と高さの値を32ビットのビット列に変換し，出力用ビット列に連結
    BitSeq Ws = To32BitCode(W);
    BitSeq Hs = To32BitCode(H);
    seq += Ws;
    seq += Hs;

    // カラーパレットの情報をビット列に変換し出力用ビット列に連結
    // 全部で 4 色あり，各色は 2 ビットコードで表されることは既知とする
    for (int i = 0; i < 4; ++i) {

        // i 番目の色とコードを取得
        int r = Palette.GetColor(i, R);
        int g = Palette.GetColor(i, G);
        int b = Palette.GetColor(i, B);
        BitSeq code = Palette.GetCode(i);

        // 色情報をビット列に変換
        BitSeq Rs = To8BitCode(r);
        BitSeq Gs = To8BitCode(g);
        BitSeq Bs = To8BitCode(b);

        // 色とコードの情報を出力用ビット列に連結
        seq += Rs;
        seq += Gs;
        seq += Bs;
        seq += code;
    }

    // 各ピクセルに対し・・・
    for (int y = 0; y < H; ++y) {
        for (int x = 0; x < W; ++x) {

            // そのピクセルの色に最も近い色をカラーパレットの中から探し，
            // 対応する 2 ビットコードを取得する
            BitSeq code = Palette.ToCode(I(x, y, R), I(x, y, G), I(x, y, B));

            // 取得した 2 ビットコードを出力用ビット列に連結
            seq += code;
        }
    }

    // 出力用ビット列をファイルに保存
    seq.Save(argv[2]);

    return 0;
}
