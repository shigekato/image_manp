#include <stdio.h>
#include "defs.h"
#include "image.h"
#include "tools.h"
#include "bitseq.h"


using namespace Sousei;
using namespace std;


// ラスタスキャンを行う関数
// なお，vector<int> の意味と使い方については tools.h を参照のこと
void RasterScan(
	const Image& I, // 画像（入力）
	vector<int>& arr) // ラスタスキャン後の一次元配列（出力）
{
	// 画像サイズを取得
	int W = I.Width();
	int H = I.Height();

	// 出力配列を空にする
	arr.clear();

	// 各ピクセルの画素値を R, G, B, R, G, B, ・・・ の順で一列に並べる
	for (int y = 0; y < H; ++y) {
		for (int x = 0; x < W; ++x) {
			arr.push_back(I(x, y, R));
			arr.push_back(I(x, y, G));
			arr.push_back(I(x, y, B));
		}
	}

	return;
}


// メイン関数： ここから処理を開始
int main(int argc, char** argv)
{
	if (argc < 3) {
		fprintf(stderr, "実行方法: ./ex11-1 [入力画像] [圧縮ファイル]\n");
		return 1;
	}

	// 入力画像を読み込む
	Image I;
	if (I.Load(argv[1]) != SOUSEI_OK) {
		fprintf(stderr, "エラー： 入力画像の読み込みに失敗しました．\n");
		return 1;
	}

	// 入力画像の幅と高さを取得
	int W = I.Width();
	int H = I.Height();

	// ラスタスキャン後のデータを格納する一次元配列を用意
	vector<int> arr;

	// 入力画像をラスタスキャン
	RasterScan(I, arr);

	// ランレングス表現されたデータを格納する配列を用意
	vector<run_length_unit> run;

	// ラスタスキャン後のデータをランレングス表現に変換
	ToRunLength(arr, run);

	// 出力用ビット列
	BitSeq seq;

	// 元画像の幅と高さ（復元時に必要）の値を32ビットのビット列に変換し，出力用ビット列に連結
	BitSeq Ws = To32BitCode(W);
	BitSeq Hs = To32BitCode(H);
	seq += Ws;
	seq += Hs;

	// ランレングス表現の長さを32ビットのビット列に変換し，出力用ビット列に連結
	int L = (int)run.size();
	BitSeq Ls = To32BitCode(L);
	seq += Ls;

	// ランレングス表現における value, count の値を8ビットのビット列に変換し，出力用ビット列に連結
	for (int i = 0; i < L; ++i) {
		BitSeq vs = To8BitCode(run[i].value);
		BitSeq cs = To8BitCode(run[i].count);
		seq += vs;
		seq += cs;
	}

	// 出力用ビット列をファイルに保存
	seq.Save(argv[2]);

	return 0;
}
