#include <stdio.h>
#include "defs.h"
#include "image.h"
#include "tools.h"
#include "bitseq.h"


using namespace Sousei;


// メイン関数： ここから処理を開始
int main(int argc, char** argv)
{
	if (argc < 3) {
		fprintf(stderr, "実行方法: ./ex12-2 [圧縮ファイル] [復元画像]\n");
		return 1;
	}

	// 入力ファイルからビット列を読み込む
	BitSeq seq;
	seq.Load(argv[1]);

	// ビット列の 0 ビット目から 32 ビット分を抜き出す ⇒ 画像の幅を表す整数値に変換
	BitSeq Ws = seq.SubSeq(0, 32);
	int W = ToInt32(Ws);

	// ビット列の 32 ビット目から 32 ビット分を抜き出す ⇒ 画像の高さを表す整数値に変換
	BitSeq Hs = seq.SubSeq(32, 32);
	int H = ToInt32(Hs);

	// 幅 W, 高さ H の復元画像を用意
	Image I(W, H);

	// カラーパレットを用意
	ColorPalette Palette;

	// ビット列からカラーパレットの情報を読み取る
	// 全部で 4 色あり，各色は 2 ビットコードで表されることは既知とする
	int m = 64;
	for (int i = 0; i < 4; ++i) {

		// 8 ビットずつ 3 つ読み出す ⇒ R, G, B の値に変換
		BitSeq Rs = seq.SubSeq(m, 8);
		int r = ToInt8(Rs);
		m += 8;
		BitSeq Gs = seq.SubSeq(m, 8);
		int g = ToInt8(Gs);
		m += 8;
		BitSeq Bs = seq.SubSeq(m, 8);
		int b = ToInt8(Bs);
		m += 8;

		// 2 ビット読み出す ⇒ 対応する 2 ビットコードとみなす
		BitSeq code = seq.SubSeq(m, 2);
		m += 2;

		// 読み出した色と 2 ビットコードのペアをカラーパレットに登録
		Palette.Set(r, g, b, code);
	}

	// 各ピクセルに対し・・・
	for (int y = 0; y < H; ++y) {
		for (int x = 0; x < W; ++x) {

			// ビット列から 2 ビット読み出す
			BitSeq code = seq.SubSeq(m, 2);
			m += 2;

			// 読み出した 2 ビットコードに対応する色をセット
			I(x, y, R) = Palette.ToColor(code, R);
			I(x, y, G) = Palette.ToColor(code, G);
			I(x, y, B) = Palette.ToColor(code, B);
		}
	}

	// 画像を表示
	I.Show();

	// 画像を保存
	I.Save(argv[2]);

	return 0;
}
