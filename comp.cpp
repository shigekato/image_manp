#include <stdio.h>
#include "defs.h"
#include "image.h"
#include "tools.h"
#include "bitseq.h"


using namespace Sousei;


// メイン関数： ここから処理を開始
int main(int argc, char** argv)
{
	if (argc < 3) {
		fprintf(stderr, "実行方法: ./ex12-1 [入力画像] [圧縮ファイル]\n");
		return 1;
	}

	// 入力画像を読み込む
	Image I;
	if (I.Load(argv[1]) != SOUSEI_OK) {
		fprintf(stderr, "エラー： 入力画像の読み込みに失敗しました．\n");
		return 1;
	}

	// 入力画像の幅と高さを取得
	int W = I.Width();
	int H = I.Height();

	// 出力用ビット列を用意
	BitSeq seq;

	// 画像の幅と高さの値を32ビットのビット列に変換し，出力用ビット列に連結
	BitSeq Ws = To32BitCode(W);
	BitSeq Hs = To32BitCode(H);
	seq += Ws;
	seq += Hs;




	/*  このあたりに具体的な圧縮処理を記述する  */




	// 出力用ビット列をファイルに保存
	seq.Save(argv[2]);

	return 0;
}

