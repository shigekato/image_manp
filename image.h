#ifndef IMAGE_H__
#define IMAGE_H__


#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <opencv2/opencv.hpp>
#include "defs.h"


namespace Sousei
{
	// カラーチャネルの定義
	enum COLOR_CHANNEL {
		R, // 赤 (Red)
		G, // 緑 (Green)
		B  // 青 (Blue)
	};

	// カラー画像を表現するクラス
	class Image : private Noncopyable
	{
	private:
		int dmy; // ダミー変数（基本的に使わない）
		int width; // 画像の幅
		int height; // 画像の高さ
		cv::Vec3i** pData; // 画素値配列へのポインタ
		cv::Mat disp; // 表示用画像

	private:
		// 画素値配列用のメモリの確保
		void alloc__(int w, int h)
		{
			release__();
			width = w;
			height = h;
			pData = new cv::Vec3i*[height];
			for (int y = 0; y < height; ++y) {
				pData[y] = new cv::Vec3i[width];
				memset(pData[y], 0, sizeof(cv::Vec3i) * width); // 全ての画素値を 0 で初期化
			}
		}

		// 画素値配列用のメモリの解放
		void release__()
		{
			if (pData != NULL) {
				for (int y = 0; y < height; ++y) {
					delete[] pData[y];
					pData[y] = NULL;
				}
				delete[] pData;
				pData = NULL;
			}
			width = height = 0;
		}

		// pDataの内容を cv::Mat 形式に変換する
		void convert__()
		{
			if (disp.cols != width || disp.rows != height) {
				disp.release();
				disp = cv::Mat::zeros(height, width, CV_8UC3);
			}
			cv::Vec3b* p = disp.ptr<cv::Vec3b>(0);
			for (int y = 0; y < height; ++y) {
				for (int x = 0; x < width; ++x, ++p) {
					(*p)[0] = cv::saturate_cast<uchar>(std::abs(pData[y][x][0]));
					(*p)[1] = cv::saturate_cast<uchar>(std::abs(pData[y][x][1]));
					(*p)[2] = cv::saturate_cast<uchar>(std::abs(pData[y][x][2]));
				}
			}
		}

	public:
		// コンストラクタ
		Image(int w = 0, int h = 0) : dmy(0), width(0), height(0), pData(NULL)
		{
			if (w > 0 && h > 0) alloc__(w, h);
		}

		// デストラクタ
		~Image()
		{
			release__();
		}

		// 幅・高さの取得
		int Width() const { return width; }
		int Height() const { return height; }

		// ファイルから画像を読み込み
		SOUSEI_ERROR_CODE Load(const std::string filename)
		{
			disp = cv::imread(filename, cv::IMREAD_COLOR);
			if (disp.data == NULL) return SOUSEI_FAILURE;
			else {
				alloc__(disp.cols, disp.rows);
				cv::Vec3b* p = disp.ptr<cv::Vec3b>(0);
				for (int y = 0; y < height; ++y) {
					for (int x = 0; x < width; ++x, ++p) {
						pData[y][x] = cv::Vec3i(
							cv::saturate_cast<int>((*p)[0]),
							cv::saturate_cast<int>((*p)[1]),
							cv::saturate_cast<int>((*p)[2])
						);
					}
				}
				return SOUSEI_OK;
			}
		}

		// 幅 w, 高さ h の画像を格納するためのメモリを確保
		// 確保したメモリは黒一色で初期化
		SOUSEI_ERROR_CODE Alloc(int w, int h)
		{
			if (w > 0 && h > 0) {
				alloc__(w, h);
				return SOUSEI_OK;
			}
			else return SOUSEI_FAILURE;
		}

		// 現在の画像を破棄
		SOUSEI_ERROR_CODE Release()
		{
			release__();
			return SOUSEI_OK;
		}

		// 現在の画像を .bmp 形式で保存
		SOUSEI_ERROR_CODE Save(const std::string filename)
		{
			if (pData != NULL) {
				std::string name = filename;
				if (name.substr(name.length() - 4, 4) != ".bmp") {
					name += ".bmp";
				}
				convert__();
				cv::imwrite(name, disp);
				return SOUSEI_OK;
			}
			else return SOUSEI_FAILURE;
		}

		// 画像を表示（何かキーが押されるまでプログラムの実行を一時中断）
		SOUSEI_ERROR_CODE Show()
		{
			if (pData != NULL) {
				convert__();
				cv::namedWindow("Viewer");
				cv::imshow("Viewer", disp);
				cv::waitKey(0);
				cv::destroyWindow("Viewer");
				return SOUSEI_OK;
			}
			else return SOUSEI_FAILURE;
		}

		// 画素値の取得・設定
		const int operator()(int x, int y, COLOR_CHANNEL c) const
		{
			if (0 <= x && x < width && 0 <= y && y < height) {
				switch (c) {
				case R:
					return pData[y][x][2];
				case G:
					return pData[y][x][1];
				case B:
					return pData[y][x][0];
				default:
					fprintf(stderr, "警告： 不正なカラーチャネルが指定されました．\n");
					return 0;
				}
			}
			else {
				fprintf(stderr, "警告： 配列外アクセスが生じました．\n");
				return 0;
			}
		}
		int& operator()(int x, int y, COLOR_CHANNEL c)
		{
			if (0 <= x && x < width && 0 <= y && y < height) {
				switch (c) {
				case R:
					return pData[y][x][2];
				case G:
					return pData[y][x][1];
				case B:
					return pData[y][x][0];
				default:
					fprintf(stderr, "警告： 不正なカラーチャネルが指定されました．\n");
					return dmy;
				}
			}
			else {
				fprintf(stderr, "警告： 配列外アクセスが生じました．\n");
				return dmy;
			}
		}
	};
}


#endif
