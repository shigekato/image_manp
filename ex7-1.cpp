#include <stdio.h>
#include "defs.h"
#include "image.h"
#include "bitseq.h"


using namespace Sousei;


// メイン関数： ここから処理を開始
int main(int argc, char** argv)
{
	const int n = 3; // [パラメータ]圧縮画像では解像度を幅・高さともに 1/n 倍する

	if (argc < 3) {
		fprintf(stderr, "実行方法: ./ex7-1 [入力画像] [圧縮ファイル]\n");
		return 1;
	}

	// 入力画像を読み込む
	Image I;
	if (I.Load(argv[1]) != SOUSEI_OK) {
		fprintf(stderr, "エラー： 入力画像の読み込みに失敗しました．\n");
		return 1;
	}

	// 入力画像の幅と高さを取得
	int W = I.Width();
	int H = I.Height();

	// 解像度を幅・高さともに 1/n にした低解像度画像を用意
	int Wo = (W + n - 1) / n; // n-1 を足しているのは小数点以下切り上げ処理のため
	int Ho = (H + n - 1) / n;
	Image S(Wo, Ho);

	// 低解像度画像の各ピクセルに対し・・・
	for (int y = 0; y < Ho; ++y) {
		for (int x = 0; x < Wo; ++x) {
			// 指定の式に従って色をセット
			S(x, y, R) = I(n * x, n * y, R);
			S(x, y, G) = I(n * x, n * y, G);
			S(x, y, B) = I(n * x, n * y, B);
		}
	}

	// 低解像度画像の表示
	S.Show();

	// 出力用ビット列
	BitSeq seq;

	// 元画像の幅と高さ（復元時に必要）の値を32ビットのビット列に変換し，出力用ビット列に連結
	BitSeq Ws = To32BitCode(W);
	BitSeq Hs = To32BitCode(H);
	seq += Ws;
	seq += Hs;

	// 低解像度画像の各ピクセルに対し・・・
	for (int y = 0; y < Ho; ++y) {
		for (int x = 0; x < Wo; ++x) {
			// R, G, B の値をそれぞれ8ビットのビット列に変換し，出力用ビット列に連結
			BitSeq Rs = To8BitCode(S(x, y, R));
			BitSeq Gs = To8BitCode(S(x, y, G));
			BitSeq Bs = To8BitCode(S(x, y, B));
			seq += Rs;
			seq += Gs;
			seq += Bs;
		}
	}

	// 出力用ビット列をファイルに保存
	seq.Save(argv[2]);

	return 0;
}
