#include <stdio.h>
#include "defs.h"
#include "image.h"


using namespace Sousei;


int get_inputs(int&, int&, int&, int&, int&);


// メイン関数： ここから処理を開始
int main(int argc, char** argv)
{
	// 画像の幅と高さ（0 で初期化）
	int W = 0;
	int H = 0;

	// 塗りつぶしの色（(0, 0, 0) で初期化）
	int R1 = 0, G1 = 0, B1 = 0;

	// W, H, (R1, G1, B1) をキーボードから入力
	get_inputs(W, H, R1, G1, B1);

	// 幅 W, 高さ H の画像を用意
	Image I(W, H);

	// 各ピクセルに対し・・・
	for (int y = 0; y < H; ++y) {
		for (int x = 0; x < W; ++x) {
			// 指定の色をセット
			I(x, y, R) = R1;
			I(x, y, G) = G1;
			I(x, y, B) = B1;
		}
	}
	
	for(int i =0; i< W; ++i){
		I(i, i, R) = 0;
		I(i, i, G) = 0;
		I(i, i, B) = 0;
	}

	// 画像を表示
	I.Show();

	return 0;
}


// 画像の幅，高さ，色をキーボードから入力する
int get_inputs(int& W, int& H, int& R1, int& G1, int& B1)
{
	const int BUF_SIZE = 256;

	int c[3] = { 0, 0, 0 };
	char buf[BUF_SIZE];

	// 幅 W の指定
	do {
		fprintf(stdout, "画像の幅(W)： ");
		if (fgets(buf, BUF_SIZE, stdin) == NULL) {
			fprintf(stdout, "error: キーボード入力の読み取りに失敗しました。\n");
			return 1;
		}
		if ((W = atoi(buf)) < 1) {
			fprintf(stdout, "1 以上の整数を入力してください。\n");
		}
		else break;
	} while (1);

	// 高さ H の指定
	do {
		fprintf(stdout, "画像の高さ(H)： ");
		if (fgets(buf, BUF_SIZE, stdin) == NULL) {
			fprintf(stdout, "error: キーボード入力の読み取りに失敗しました。\n");
			return 1;
		}
		if ((H = atoi(buf)) < 1) {
			fprintf(stdout, "1 以上の整数を入力してください。\n");
		}
		else break;
	} while (1);

	// 色 (R1, G1, B1) の指定
	for (int i = 0; i < 3; ++i) {
		do {
			switch (i) {
			case 0:
				fprintf(stdout, "色(R1)： ");
				break;
			case 1:
				fprintf(stdout, "色(G1)： ");
				break;
			default:
				fprintf(stdout, "色(B1)： ");
			}
			if (fgets(buf, BUF_SIZE, stdin) == NULL) {
				fprintf(stdout, "error: キーボード入力の読み取りに失敗しました。\n");
				return 1;
			}
			if ((c[i] = atoi(buf)) < 0 || 255 < c[i]) {
				fprintf(stdout, "0 以上 255 以下の整数を入力してください。\n");
			}
			else break;
		} while (1);
	}
	R1 = c[0];
	G1 = c[1];
	B1 = c[2];

	return 0;
}
