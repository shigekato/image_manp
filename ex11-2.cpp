#include <stdio.h>
#include "defs.h"
#include "image.h"
#include "tools.h"
#include "bitseq.h"


using namespace Sousei;
using namespace std;


// ラスタスキャン後の一次元配列を画像に戻す関数
void InverseRasterScan(
	const vector<int>& arr, // ラスタスキャン後の一次元配列（入力）
	Image& I) // 画像（出力）
{
	// 画像サイズを取得
	int W = I.Width();
	int H = I.Height();

	// 一次元配列中の整数値を R, G, B, R, G, B, ・・・ の順で各ピクセルにセット
	int m = 0;
	for (int y = 0; y < H; ++y) {
		for (int x = 0; x < W; ++x) {
			I(x, y, R) = arr[m++]; // I(x, y, R) = arr[m]; の後で m++; を行うのと同じ（代入した後に m を 1 増やす）
			I(x, y, G) = arr[m++];
			I(x, y, B) = arr[m++];
		}
	}

	return;
}


// メイン関数： ここから処理を開始
int main(int argc, char** argv)
{
	if (argc < 3) {
		fprintf(stderr, "実行方法: ./ex11-2 [圧縮ファイル] [復元画像]\n");
		return 1;
	}

	// 入力ファイルからビット列を読み込む
	BitSeq seq;
	seq.Load(argv[1]);

	// ビット列の 0 ビット目から 32 ビット分を抜き出す ⇒ 元画像の幅を表す整数値に変換
	BitSeq Ws = seq.SubSeq(0, 32);
	int W = ToInt32(Ws);

	// ビット列の 32 ビット目から 32 ビット分を抜き出す ⇒ 元画像の高さを表す整数値に変換
	BitSeq Hs = seq.SubSeq(32, 32);
	int H = ToInt32(Hs);

	// ビット列の 64 ビット目から 32 ビット分を抜き出す ⇒ ランレングス表現の長さを表す整数値に変換
	BitSeq Ls = seq.SubSeq(64, 32);
	int L = ToInt32(Ls);

	// 長さ L のランレングス表現を格納する配列を用意
	vector<run_length_unit> run(L);

	// ビット列の 96 ビット目以降からランレングス表現されたデータを読み出す
	int m = 96;
	for (int i = 0; i < L; ++i) {
		BitSeq vs = seq.SubSeq(m, 8);
		m += 8;
		BitSeq cs = seq.SubSeq(m, 8);
		m += 8;
		run[i].value = ToInt8(vs);
		run[i].count = ToInt8(cs);
	}

	// 復元データを格納するための一次元配列を用意
	vector<int> arr;

	// ランレングス表現を一次元配列に戻す
	ToArray(run, arr);

	// 幅 W, 高さ H の復元画像を用意
	Image I(W, H);

	// 一次元配列を画像に戻す
	InverseRasterScan(arr, I);

	// 画像を表示
	I.Show();

	// 画像を保存
	I.Save(argv[2]);

	return 0;
}
